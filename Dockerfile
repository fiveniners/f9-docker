FROM ubuntu:22.04
MAINTAINER Fiveniners

RUN apt-get update && \
    apt-get upgrade -y

RUN apt-get install -y --no-install-recommends \
    zip \
    curl \
    unzip \
    groff \
    jq \
    python3-pip \
    python-setuptools \
    ca-certificates
    
RUN pip3 install awscli
RUN aws --version
RUN jq --version

RUN curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
RUN echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list
RUN apt-get update
RUN apt-get install -y kubectl
RUN kubectl version || true
